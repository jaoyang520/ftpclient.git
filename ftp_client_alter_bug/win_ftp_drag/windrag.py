from tkinter.messagebox import showinfo

import windnd
from ftp_client_alter_bug.tree.win_tree import WinTree


class WinDrag():
    def __init__(self, left_frame, win_path, xuanzhe_win_path, tree):
        self.left_frame = left_frame
        self.win_path = win_path
        self.xuanzhe_win_path = xuanzhe_win_path
        self.tree = tree

    def win_file(self, files):
        msg = "\n".join((item.decode("gbk") for item in files))
        print(msg)
        try:
            with open(msg, mode="rb+") as re:
                buffer = re.read()
            with open(self.win_path[0] + "\\" + msg.split("\\")[-1], mode="wb+") as wr:
                wr.write(buffer)
            table = self.tree
            items = table.get_children()
            [table.delete(item) for item in items]

            wintree = WinTree(self.xuanzhe_win_path[0], self.win_path, self.tree)
            wintree.show_wenjian()
        except:
            showinfo("错误提示", "未选择本地路径！")

    def execute(self):
        windnd.hook_dropfiles(self.left_frame, func=self.win_file)