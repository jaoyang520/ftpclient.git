import os
import tkinter as tk
from tkinter import ttk
from ftplib import FTP  # 加载ftp模块
import sys

class FtpTree:

    def __init__(self, ftp_path, ftp_list, tree_ftp):

        self.tree = tree_ftp
        self.num = 0

        self.ftp_list = ftp_list
        self.ftp_path = ftp_path


        # # 滚动条
        # sy = tk.Scrollbar(self.right_frame)
        # sy.pack(side=tk.LEFT, fill=tk.Y)
        # sy.config(command=self.tree.yview)
        # self.tree.config(yscrollcommand=sy.set)

        # 绑定事件
        self.tree.bind("<<TreeviewSelect>>", self.func)
        self.tree.pack(expand=True, fill=tk.BOTH)
        self.ev = tk.Variable()
        print(self.ev)

    def xianshi(self, path_ftp):

        self.ftp = self.ftp_list[0]
        self.ftp.cwd(path_ftp)
        self.num += 1
        dir_res = []
        self.ftp.dir('.', dir_res.append)
        for i in dir_res:
            if i.split()[2] == "<DIR>":
                self.tree.insert("", "end", text=i.split()[3], open=True, values=(i.split()[0], i.split()[2]))
            else:
                self.tree.insert("", "end", text=i.split()[3], open=True, values=(i.split()[0], "<file>"))

    def func(self, event):
        # widget触发事件的构建
        self.v = event.widget.selection()
        for sv in self.v:
            self.file = self.tree.item(sv)["text"]
            self.ev.set(self.file)

            self.ftp_path.clear()
            self.ftp_path.append(self.file)
            print(self.ftp_path)
            print(self.file)


