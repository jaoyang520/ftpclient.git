import os
import tkinter as tk
from tkinter import ttk

class WinTree:

    def __init__(self, path, win_path, tree):
        self.path = path
        self.win_path = win_path
        self.tree = tree

        # # 滚动条
        # sy = tk.Scrollbar(self.left_frame)
        # sy.pack(side=tk.RIGHT, fill=tk.Y)
        # sy.config(command=self.tree.yview)
        # self.tree.config(yscrollcommand=sy.set)

        # 绑定事件
        self.tree.bind("<<TreeviewSelect>>", self.func)
        self.tree.pack(expand=True, fill=tk.BOTH)
        self.ev = tk.Variable()
        print(self.ev)

    def show_wenjian(self):
        self.root = self.tree.insert("", "end", text=self.getlastpath(self.path, ), open=True, values=(self.path,))
        self.loadtree(self.root, self.path)

    def func(self, event):
        # widget触发事件的构建
        self.v = event.widget.selection()
        for sv in self.v:
            file = self.tree.item(sv)["text"].replace('/', '\\')
            self.ev.set(file)
            apath = self.tree.item(sv)["values"][0].replace('/', '\\')

            self.win_path.clear()
            self.win_path.append(apath)
            print(self.win_path)
            print(apath)

    def loadtree(self, parent, parentPath):
        for FileName in os.listdir(parentPath):
            absPath = os.path.join(parentPath, FileName)
            if ("$" in absPath) == False:
                # 插入树枝
                treey = self.tree.insert(parent, "end", text=self.getlastpath(absPath), values=(absPath, ))

                # 判断是否是目录
                if os.path.isdir(absPath):

                    self.loadtree(treey, absPath)

    def getlastpath(self, path):

        pathList = os.path.split(path)
        return pathList[-1]
