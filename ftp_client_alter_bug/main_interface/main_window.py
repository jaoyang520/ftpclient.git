import tkinter as tk
from tkinter import ttk
from ftp_client_alter_bug.bottom_menu.bottom_menu_bar_center import BottomMenuBarCenter
from ftp_client_alter_bug.seek.win_search import WinSearch
from ftp_client_alter_bug.top_menu.top_menu_bar import TopMenuBar
from ftp_client_alter_bug.bottom_menu.bottom_menu_bar_left import BottomMenuBarleft
from ftp_client_alter_bug.bottom_menu.bottom_menu_bar_right import BottomMenuBarRight
from ftp_client_alter_bug.tree.win_tree import WinTree
from ftp_client_alter_bug.tree.ftp_tree import FtpTree
from ftp_client_alter_bug.win_ftp_drag.ftpdrag import FtpDrag
from ftp_client_alter_bug.win_ftp_drag.windrag import WinDrag

win = tk.Tk()
win.title("FTP客户端")
win.geometry("1200x700+120+60")
win.resizable(0, 0)


xuanzhe_win_path = []

win_path = []
ftp_path = []
win_search_name = []
win_search_path = []
ftp_search_path = []
ftp_list = []
ftp_tree_list = []
win_xuanze_tree = []
win_search_tree = []
path = "初始化占位"

top_frame = tk.Frame(win, width="1200", height="60")
top_frame.place(x=0, y=5)
top_menu = TopMenuBar(top_frame, path, win, win_search_path, ftp_list, xuanzhe_win_path)
top_menu.logo()

top_menu.open_wenjian()
# top_menu.search_box()
top_menu.name_ip("", "")
top_menu.exit_login(ftp_tree_list, ftp_path, ftp_list)
top_menu.login_in(ftp_path, ftp_list, ftp_tree_list)
top_menu.lishi_login_in(ftp_path, ftp_list, ftp_tree_list)


left_frame = tk.Frame(win, width="350", height="540")
left_frame.place(x=10, y=90)
tree = ttk.Treeview(left_frame, show="tree", height=26)
tree.column("#0", width=350)
st = ttk.Style()
st.configure("Treeview", font=("宋体", 13))
winftpdrag = WinDrag(left_frame, win_path, xuanzhe_win_path, tree)
winftpdrag.execute()

win_xuanze_tree.clear()
win_xuanze_tree.append(tree)

wintree = WinTree(path, win_path, tree)

top_menu.local_mulu(win_path, tree)



# 创建两个一模一样的frame实现一个注销一个加载
win_center_frame = tk.Frame(win, width="420",bg="green", height="540")
win_center_frame.place(x=350, y=90)
# search_path = r"D:\\python_daima"
search_name = "9999"

win_tree = ttk.Treeview(win_center_frame, show="tree", height=26)

win_tree.column("#0", width=440)
st = ttk.Style()
st.configure("Treeview", font=("宋体", 12))
win_search_tree.clear()
win_search_tree.append(win_tree)

winsearch = WinSearch(path, search_name, win_search_path, win_tree)

top_menu.search_box(win_search_name, win_search_tree, ftp_search_path, ftp_list)



right_frame = tk.Frame(win, width="420", height="540")
right_frame.place(x=790, y=90)

tree_ftp = ttk.Treeview(right_frame, show="tree", height=26, columns=("shijian", "wenjiantype"))
tree_ftp.column("#0", width=250)
tree_ftp.column("shijian", width=80)
tree_ftp.column("wenjiantype", width=60)

ftp_tree_list.append(tree_ftp)

st = ttk.Style()
st.configure("Treeview", font=("宋体", 13))


bommot_frame_left = tk.Frame(win, width="350", height="60")
bommot_frame_left.place(x=0, y=635)
bommot_menu_left = BottomMenuBarleft(bommot_frame_left)
bommot_menu_left.win_chuangjian_mulu(win_path, xuanzhe_win_path, tree)
bommot_menu_left.win_chuangjian_wenjian(win_path, xuanzhe_win_path, tree)
bommot_menu_left.win_delete_mulu(win_path, xuanzhe_win_path, tree)
bommot_menu_left.win_delete_wenjian(win_path, xuanzhe_win_path, tree)

# bommot_menu_left.shangchaung(win, ftp_path, ftp_list, ftp_frame, ftp_tree, win_path, bommot_frame_center)
bommot_menu_left.open_wenjian(win_path)
bommot_menu_left.gexian()


bommot_frame_center = tk.Frame(win,  width="500", height="60")
bommot_frame_center.place(x=350, y=635)
bottom_menu_center = BottomMenuBarCenter(bommot_frame_center)
bottom_menu_center.delete_mulu(win_search_path, xuanzhe_win_path, win_search_name, win_tree, win_path, tree)
bottom_menu_center.open_weizhi(win_search_path)
bottom_menu_center.delete_wenjian(ftp_list, tree_ftp, ftp_path, win_search_name, win_search_tree, ftp_search_path)
bottom_menu_center.xiazai_wenjian(ftp_list, win_path, xuanzhe_win_path, bommot_frame_center, tree, ftp_search_path)
bottom_menu_center.gexian()

ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
ftpdrag = FtpDrag(right_frame, ftp_path, ftp_list, win_path, bommot_frame_center, tree_ftp)
ftpdrag.execute()


bommot_menu_left.shangchaung(ftp_path, ftp_list, win_path, bommot_frame_center, tree_ftp)

bommot_frame_right = tk.Frame(win, width="350", height="60")
bommot_frame_right.place(x=850, y=635)
bommot_menu_right = BottomMenuBarRight(bommot_frame_right)
bommot_menu_right.geban()
bommot_menu_right.ftp_chaungjian_mulu(ftp_path, ftp_list, tree_ftp)
bommot_menu_right.ftp_jinru_xj(ftp_path, ftp_list, tree_ftp)
bommot_menu_right.ftp_delete_mulu(ftp_path, ftp_list, tree_ftp)
bommot_menu_right.ftp_delete_wenjian(ftp_path, ftp_list, tree_ftp)
bommot_menu_right.xiazai(ftp_list, ftp_path, win_path, xuanzhe_win_path, bommot_frame_center, tree)
bommot_menu_right.ftp_fh_sj(ftp_path, ftp_list, tree_ftp)


tk.mainloop()