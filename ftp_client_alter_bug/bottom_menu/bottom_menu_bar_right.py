import tkinter as tk
from tkinter.messagebox import showinfo

from ftp_client_alter_bug.tree.ftp_tree import FtpTree
from ftp_client_alter_bug.tree.win_tree import WinTree


class BottomMenuBarRight:

    def __init__(self, bommot_frame_right):
        self.bommot_frame_right = bommot_frame_right

    # 隔板
    def geban(self):
        tk.Label(self.bommot_frame_right, text="|f\n|t\n|p", font=("宋体", 13), relief="flat").place(x=-5, y=5)

    # 创建目录
    def ftp_chaungjian_mulu(self, ftp_path, ftp_list, tree_ftp):

        def ftp_cj_mulu_button():
            cj_win = tk.Tk()
            cj_win.geometry("300x200+500+350")
            titlte_lab = tk.Label(cj_win, text="创建目录", font=("宋体", 18))
            titlte_lab.place(x=105, y=25)

            titlte_lab = tk.Label(cj_win, text="目录名:", font=("宋体", 13))
            titlte_lab.place(x=30, y=80)

            cj_entry = tk.Entry(cj_win, width=21)
            cj_entry.place(x=100, y=80)

            def cj_button():
                try:
                    muluzhi = str(cj_entry.get()).encode()
                    ftp_list[0].mkd(muluzhi.decode())
                    print(ftp_path)
                except:
                    showinfo("错误提示", "路径选择或服务器连接异常！")
                finally:
                    cj_win.destroy()


                if str(ftp_list[0].pwd().encode('iso-8859-1').decode('GB18030')) != "/":
                    table = tree_ftp
                    items = table.get_children()
                    [table.delete(item) for item in items]
                    ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                    ftptree.xianshi(ftp_list[0].pwd().encode('iso-8859-1').decode('GB18030'))
                    # cj_win.destroy()
                else:
                    table = tree_ftp
                    items = table.get_children()
                    [table.delete(item) for item in items]
                    ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                    ftptree.xianshi("/")
                    # cj_win.destroy()


            cj_but = tk.Button(cj_win, text="确=======================认", font=("宋体", 13), command=cj_button)
            cj_but.place(x=20, y=130)

            cj_win.mainloop()

        ftp_cj_mulu_but = tk.Button(self.bommot_frame_right, text="创建目录", font=("宋体", 13), relief="flat", command=ftp_cj_mulu_button)
        ftp_cj_mulu_but.place(x=40, y=2)


    # 进入下级
    def ftp_jinru_xj(self, ftp_path, ftp_list, tree_ftp):
        def ftp_jinru_xj_button():
            try:
                global path
                path = ftp_path[0]
                print(path)

                table = tree_ftp
                items = table.get_children()
                [table.delete(item) for item in items]
                ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                ftptree.xianshi(ftp_path[0])
            except:
                showinfo("错误提示", "路径选择或服务器连接异常！")

        global a
        a = ftp_jinru_xj_button

        ftp_jinru_xj_but = tk.Button(self.bommot_frame_right, text="进入下级", font=("宋体", 13), relief="flat", command=ftp_jinru_xj_button)
        ftp_jinru_xj_but.place(x=40, y=35)

    # 删除目录
    def ftp_delete_mulu(self, ftp_path, ftp_list, tree_ftp):
        def ftp_delete_mulu_button():
            try:
                ftp_list[0].rmd(ftp_path[0])

                if str(ftp_list[0].pwd().encode('iso-8859-1').decode('GB18030')) != "/":
                    table = tree_ftp
                    items = table.get_children()
                    [table.delete(item) for item in items]
                    ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                    ftptree.xianshi(ftp_list[0].pwd().encode('iso-8859-1').decode('GB18030'))
                else:
                    table = tree_ftp
                    items = table.get_children()
                    [table.delete(item) for item in items]
                    ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                    ftptree.xianshi("/")
            except:
                showinfo("错误提示", "路径选择或服务器连接异常！")


        ftp_delete_mulu_but = tk.Button(self.bommot_frame_right, text="删除目录", font=("宋体", 13), relief="flat", command=ftp_delete_mulu_button)
        ftp_delete_mulu_but.place(x=150, y=2)

    # 删除文件
    def ftp_delete_wenjian(self, ftp_path, ftp_list, tree_ftp):
        def ftp_delete_wenjian_button():
            try:

                ftp_list[0].delete(ftp_path[0])

                if str(ftp_list[0].pwd().encode('iso-8859-1').decode('GB18030')) != "/":
                    table = tree_ftp
                    items = table.get_children()
                    [table.delete(item) for item in items]
                    ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                    ftptree.xianshi(ftp_list[0].pwd().encode('iso-8859-1').decode('GB18030'))
                else:
                    table = tree_ftp
                    items = table.get_children()
                    [table.delete(item) for item in items]
                    ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                    ftptree.xianshi("/")
            except:
                showinfo("错误提示", "路径选择或服务器连接异常！")


        ftp_delete_wenjian_but = tk.Button(self.bommot_frame_right, text="删除文件", font=("宋体", 13), relief="flat", command=ftp_delete_wenjian_button)
        ftp_delete_wenjian_but.place(x=150, y=35)

    # 下载文件
    def xiazai(self, ftp_list, ftp_path, win_path, xuanzhe_win_path, bommot_frame_center, tree):

        def xiazai_button():

            # 更新进度条函数
            def change_schedule(now_schedule, all_schedule):
                canvas.coords(fill_rec, (5, 5, 6 + (now_schedule / all_schedule) * 100, 25))
                bommot_frame_center.update()
                x.set(str(round(now_schedule / all_schedule * 100, 2)) + '%')
                if round(now_schedule / all_schedule * 100, 2) == 100.00:
                    x.set("完==成==了")
            try:

                print(ftp_list[0], ftp_path[0], win_path[0], "三个路径————————")

                def downloadfile(ftp, remotepath, localpath):
                    bufsize = 1024  # 设置缓冲块大小
                    fp = open(localpath, 'wb+')  # 以写模式在本地打开文件
                    ftp.retrbinary('RETR ' + remotepath, fp.write, bufsize)  # 接收服务器上文件并写入本地文件
                    ftp.set_debuglevel(0)  # 关闭调试
                    fp.close()  # 关闭文件

                print('22222211111111111222222111', win_path[0]+"\\"+ftp_path[0].split("/")[-1])
                downloadfile(ftp_list[0], ftp_path[0].encode('GB18030').decode('GB18030'), win_path[0]+"\\"+ftp_path[0].split("\\")[-1].encode('GB18030').decode('GB18030'))
                print('11111111111111')



                canvas = tk.Canvas(bommot_frame_center, width=110, height=30, bg="white")
                canvas.place(x=150, y=25)
                x = tk.StringVar()
                # 进度条以及完成程度
                fill_rec = canvas.create_rectangle(5, 5, 5, 25, outline="", width=0, fill="blue")

                tk.Label(bommot_frame_center, textvariable=x, font=("宋体", 13)).place(x=280, y=30)

                for i in range(100):
                    change_schedule(i, 99)

                table = tree
                items = table.get_children()
                [table.delete(item) for item in items]

                wintree = WinTree(xuanzhe_win_path[0], win_path, tree)
                wintree.show_wenjian()

                print(win_path[0]+ftp_path[0].split("\\")[-1],"============================")

            except Exception as e:
                if '550' in str(e):
                    canvas = tk.Canvas(bommot_frame_center, width=110, height=30, bg="white")
                    canvas.place(x=150, y=25)
                    x = tk.StringVar()
                    # 进度条以及完成程度
                    fill_rec = canvas.create_rectangle(5, 5, 5, 25, outline="", width=0, fill="blue")

                    tk.Label(bommot_frame_center, textvariable=x, font=("宋体", 13)).place(x=280, y=30)

                    for i in range(100):
                        change_schedule(i, 99)

                    table = tree
                    items = table.get_children()
                    [table.delete(item) for item in items]

                    wintree = WinTree(xuanzhe_win_path[0], win_path, tree)
                    wintree.show_wenjian()
                else:
                    print(e)
                    showinfo("错误提示", "路径选择或服务器连接异常！")
        xiazai_but = tk.Button(self.bommot_frame_right, text="下载文件", font=("宋体", 13), relief="flat", command=xiazai_button)
        xiazai_but.place(x=250, y=2)

    # 返回上级
    def ftp_fh_sj(self, ftp_path, ftp_list, tree_ftp):

        def ftp_fh_sj_button():
            try:
                table = tree_ftp
                items = table.get_children()
                [table.delete(item) for item in items]
                ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                ftptree.xianshi("..")
            except:
                showinfo("错误提示", "路径选择或服务器连接异常！")
        global b
        b = ftp_fh_sj_button


        ftp_fh_sj_but = tk.Button(self.bommot_frame_right, text="返回上级", font=("宋体", 13), relief="flat", command=ftp_fh_sj_button)
        ftp_fh_sj_but.place(x=250, y=35)