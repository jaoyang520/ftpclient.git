import tkinter as tk
import os
from tkinter.messagebox import showinfo

from ftp_client_alter_bug.tree.ftp_tree import FtpTree
from ftp_client_alter_bug.tree.win_tree import WinTree
from ftp_client_alter_bug.seek.ftp_search import FtpSearch
from ftp_client_alter_bug.seek.win_search import WinSearch


class BottomMenuBarCenter:
    def __init__(self, bommot_frame_center):
        self.bommot_frame_center = bommot_frame_center

    # win删除文件
    def delete_mulu(self, win_search_path, xuanzhe_win_path, win_search_name, win_tree, win_path, tree):
        def delete_mulu_button():
            try:
                # 也可以递归删除文件
                def delAll(path):
                    if os.path.isdir(path):
                        files = os.listdir(path)
                        # 遍历并删除文件
                        for file in files:
                            p = os.path.join(path, file)
                            if os.path.isdir(p):
                                # 递归
                                delAll(p)
                            else:
                                os.remove(p)
                        # 删除文件夹
                        os.rmdir(path)
                    else:
                        os.remove(path)

                delAll(win_search_path[0])

                table = win_tree
                items = table.get_children()
                [table.delete(item) for item in items]

                win_name = win_search_name[0]
                winsearch = WinSearch(xuanzhe_win_path[0], win_name, win_search_path, win_tree)
                winsearch.search()

                table = tree
                items = table.get_children()
                [table.delete(item) for item in items]

                wintree = WinTree(xuanzhe_win_path[0], win_path, tree)
                wintree.show_wenjian()
            except:
                showinfo("错误提示", "路径选择异常！")


        delete_mulu_but = tk.Button(self.bommot_frame_center, text="删除文件", command=delete_mulu_button, font=("宋体", 13), relief="flat")
        delete_mulu_but.place(x=15, y=2)

    # 打开window上文件所在的位置
    def open_weizhi(self, win_search_path):
        def open_weizhi_button():
            try:
                os.startfile(rf"{win_search_path[0]}")
            except:
                showinfo("错误提示", "未选择路径！")
        open_weizhi_but = tk.Button(self.bommot_frame_center, text="打开文件", command=open_weizhi_button, font=("宋体", 13), relief="flat")
        open_weizhi_but.place(x=15, y=35)

    # ftp删除文件
    def delete_wenjian(self, ftp_list, tree_ftp, ftp_path, win_search_name, win_search_tree, ftp_search_path):
        def delete_wenjian_button():
            try:
                ftp_list[0].delete(ftp_search_path[0])

                if str(ftp_list[0].pwd()) != "/":
                    table = tree_ftp
                    items = table.get_children()
                    [table.delete(item) for item in items]
                    ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                    ftptree.xianshi(ftp_list[0].pwd())
                else:
                    table = tree_ftp
                    items = table.get_children()
                    [table.delete(item) for item in items]
                    ftptree = FtpTree(ftp_path, ftp_list, tree_ftp)
                    ftptree.xianshi("/")

                win_name = win_search_name[0]
                table = win_search_tree[0]
                items = table.get_children()
                [table.delete(item) for item in items]
                ftpsearch = FtpSearch(ftp_list[0], win_name, ftp_search_path, table)
                ftpsearch.show()
            except:
                showinfo("错误提示", "路径选择或服务器连接异常！")

        delete_wenjian_but = tk.Button(self.bommot_frame_center, text="删除文件", command=delete_wenjian_button, font=("宋体", 13), relief="flat")
        delete_wenjian_but.place(x=415, y=2)

    # ftp下载文件
    def xiazai_wenjian(self, ftp_list, win_path, xuanzhe_win_path, bommot_frame_center, tree, ftp_search_path):
        def xiazai_wenjian_button():
            try:
                print(ftp_list[0], ftp_search_path[0], win_path[0], "三个路径————————")

                def downloadfile(ftp, remotepath, localpath):
                    bufsize = 1024  # 设置缓冲块大小
                    fp = open(localpath, 'wb')  # 以写模式在本地打开文件
                    ftp.retrbinary('RETR ' + remotepath, fp.write, bufsize)  # 接收服务器上文件并写入本地文件
                    ftp.set_debuglevel(0)  # 关闭调试
                    fp.close()  # 关闭文件

                # downloadfile(ftp_list[0], ftp_search_path[0], win_path[0]+"\\"+ftp_search_path[0].split("/")[-1])
                downloadfile(ftp_list[0], ftp_search_path[0].encode('GB18030').decode('GB18030'),
                             win_path[0] + "\\" + ftp_search_path[0].split("\\")[-1].encode('GB18030').decode('GB18030'))
                # 更新进度条函数
                def change_schedule(now_schedule, all_schedule):
                    canvas.coords(fill_rec, (5, 5, 6 + (now_schedule / all_schedule) * 100, 25))
                    bommot_frame_center.update()
                    x.set(str(round(now_schedule / all_schedule * 100, 2)) + '%')
                    if round(now_schedule / all_schedule * 100, 2) == 100.00:
                        x.set("完==成==了")

                canvas = tk.Canvas(bommot_frame_center, width=110, height=30, bg="white")
                canvas.place(x=150, y=25)
                x = tk.StringVar()
                # 进度条以及完成程度
                out_rec = canvas.create_rectangle(5, 5, 105, 25, outline="blue", width=1)
                fill_rec = canvas.create_rectangle(5, 5, 5, 25, outline="", width=0, fill="blue")

                tk.Label(bommot_frame_center, textvariable=x, font=("宋体", 13)).place(x=280, y=30)

                for i in range(100):
                    change_schedule(i, 99)

                table = tree
                items = table.get_children()
                [table.delete(item) for item in items]

                wintree = WinTree(xuanzhe_win_path[0], win_path, tree)
                wintree.show_wenjian()

                print(win_path[0]+ftp_search_path[0].split("\\")[-1],"============================")
            except:
                showinfo("错误提示", "本地路径或服务器连接异常！")

        xiazai_wenjian_but = tk.Button(self.bommot_frame_center, text="下载文件", command=xiazai_wenjian_button, font=("宋体", 13), relief="flat")
        xiazai_wenjian_but.place(x=415, y=35)

    def gexian(self):
        tk.Label(self.bommot_frame_center, text="-------------进-------------度--------------条-------------").place(x=105, y=0)