import os
from tkinter import ttk
import tkinter as tk

class FtpSearch:
    def __init__(self, tree_ftp, search_name, ftp_search_path, win_tree):
        self.ftp = tree_ftp
        # 搜索框的内容
        self.search_name = search_name
        self.ftp_search_path = ftp_search_path
        self.tree = win_tree

        # # 滚动条
        # sy = tk.Scrollbar(self.center_frame)
        # sy.pack(side=tk.RIGHT, fill=tk.Y)
        # sy.config(command=self.tree.yview)
        # self.tree.config(yscrollcommand=sy.set)

        # 绑定事件
        self.tree.bind("<<TreeviewSelect>>", self.func)
        self.tree.pack(expand=True, fill=tk.BOTH)
        self.ev = tk.Variable()


    def show(self):
        dir_res = []
        self.ftp.dir('.', dir_res.append)
        for i in dir_res:
            print(i)
            if self.search_name in i.split()[3]:
                self.tree.insert("", "end", text=i.split()[3], open=True, values=(i.split()[3]))


    def func(self, event):
        # widget触发事件的构建
        self.v = event.widget.selection()
        for sv in self.v:
            file = self.tree.item(sv)["text"]
            self.ev.set(file)
            apath = self.tree.item(sv)["values"][0]

            self.ftp_search_path.clear()
            self.ftp_search_path.append(apath)
            print(self.ftp_search_path)
            print(apath)