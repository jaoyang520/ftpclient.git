# python实现可视化的ftp客户端

#### 介绍
我制作的项目是一个可视化ftp客户端，它包含了本地文件和ftp服务器文件的展示和搜索，以及上传下载和增删改查的功能。在这个项目中我运用了tkinter模块，os模块，ftplib模块，windnd模块与pickle模块中的知识，在加上python中的异常捕获处理，文件处理，socket通信等基础知识完成了此次的项目。

#### 软件架构
软件架构说明


#### 安装教程

1.  main_window为主程序入口
2.  需要在本地的系统开通一个ftp服务器
3.  xxxx

#### 使用说明

1.  ftp服务器会出现中文目录无法使用的情况，底层显示为乱码，还在持续修改中， 但是可以正常使用，只是会有异常抛出，如有大佬可以帮忙修改一下。
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
