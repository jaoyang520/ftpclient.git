# python实现可视化的ftp客户端

#### Description
我制作的项目是一个可视化ftp客户端，它包含了本地文件和ftp服务器文件的展示和搜索，以及上传下载和增删改查的功能。在这个项目中我运用了tkinter模块，os模块，ftplib模块，windnd模块与pickle模块中的知识，在加上python中的异常捕获处理，文件处理，socket通信等基础知识完成了此次的项目。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
